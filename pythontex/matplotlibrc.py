import matplotlib as mpl
mpl.rcParams['font.size'] = 22
mpl.rcParams['axes.labelsize'] = "medium"
mpl.rcParams['xtick.labelsize'] = "small"
mpl.rcParams['ytick.labelsize'] = "small"
mpl.rcParams['axes.labelcolor'] = "0.4"
mpl.rcParams['xtick.color'] = "0.4"
mpl.rcParams['ytick.color'] = "0.4"
mpl.rcParams['ytick.direction'] = "in"
mpl.rcParams['savefig.bbox'] = "tight"
mpl.rcParams['savefig.dpi'] = 400
mpl.rcParams['text.usetex'] = True
mpl.rcParams['pgf.texsystem'] = "pdflatex"
mpl.rcParams['text.latex.preamble'] = r"\usepackage{siunitx}"
