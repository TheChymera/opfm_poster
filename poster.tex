\documentclass{beamer}
\usepackage[orientation=portrait,size=custom,width=91,height=91]{beamerposter}
\mode<presentation>{\usetheme{ZH_square}}
% \usepackage{chemformula}
\usepackage[utf8]{inputenc}
\usepackage{multicol}
% \usepackage[german, english]{babel} % required for rendering German special characters
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage[autoprint=false, gobble=auto]{pythontex} % create figures on-line directly from python!
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage[font=scriptsize,justification=justified]{caption}

% \sisetup{per=frac,fraction=sfrac}
\sisetup{per-mode=symbol}

\input{pythontex/functions.tex}
\input{pythontex/pycode.tex}

\usepackage{array,booktabs,tabularx}
\newcolumntype{Z}{>{\centering\arraybackslash}X} % centered tabularx columns

\title{LONGITUDINAL OPTO-PHARMACO-FMRI OF\\\vspace{.3em} SELECTIVE SEROTONIN REUPTAKE INHIBITION}
% \title{Longitudinal opto-pharmaco-fMRI of Selective\\ Serotonin Reuptake Inhibition}
\author{Horea-Ioan Ioanas$^{1}$, Bechara Saab$^{2}$, Markus Rudin$^{1}$}
\institute[ETH]{$^{1}$Institute for Biomedical Engineering, ETH and University of Zurich \\ $^{2}$Preclinical Laboratory for Translational Research into Affective Disorders, DPPP, Psychiatric Hospital, University of Zurich}
\date{\today}

\newlength{\columnheight}
\setlength{\columnheight}{0.881\textheight}

\begin{document}
\begin{frame}
\begin{columns}
	\begin{column}{.43\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth}  % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{Background}
						The ascending serotonergic system is of great clinical importance due to its implication in affective disorders and their treatment.
						The origin of the majority of ascending serotonergic projections, the dorasl raphe (DR) represents a node with high degree centrality in the brain, and is evolutionarily well conserved.
						As such it is interesting to both applied and basic research, and excellently suited for translational study.
						\vspace{0.8em}

						Selective Serotonin Reuptake Inhibitors (SSRIs) are the foremost drug class for treating depression, and also find significant use in treating anxiety, phobia, panic, and obsessive-compulsive disorder \cite{Baldwin2005}.
						Given this prominent role of SSRIs, there have been substantial efforts in trying to understand their modulatory effects on the serotonergic system.
						Current explanations, however, remain tentative, in part due to the lack of means to longitudinally study serotonergic neurotransmission at the whole-brain-level.
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Animals and Timetables}
						DR serotonergic cells were targeted for optogenetic stimulation by injection of a Cre-conditional channelrhodopsin-expressing adeno-associated virus into C57BL/6 mice expressing Cre under the ePet promoter \cite{Scott2005}.
						An optic fiber was chronically implanted to allow for longitudinal stimulation.
						\vspace{0.4em}
						\begin{figure}
							\begin{minipage}{.94\textwidth}
								\centering\includegraphics[width=\textwidth]{img/og.png}
								\caption{\textbf{(a)} Optic fiber implant targeted at the dorsal raphe (DR), the ascending projections of which innervate numerous cortical and subcortical areas (adapted from \cite{Oegren2008}). Histological validation of the ChR2 construct expression: \textbf{(b)} localized to the DR  and \textbf{(c)} colocalized with serotonin (data from Saab and colleagues, unpublished).}
							\end{minipage}
						\end{figure}
						\vspace{0.4em}
						Longitudinal designs implemented varying SSRI (fluoxetine) administration routes:
						\begin{itemize}
							\item Intravenous (i.v.) at \SI{10}{\mg\per\kg} daily (in cohorts from figures~\ref{fig:fluip} and ~\ref{fig:fludw}).
							\item Intraperitoneal (i.p.) at \SI{10}{\mg\per\kg} daily (cohort timetable shown in figure~\ref{fig:fluip}).
							\item Drinking water (d.w.) at \SI{\approx 20}{\milli\gram\per\kilo\gram} per day (one cohort timetable shown in figure~\ref{fig:fludw}).
						\end{itemize}

						\vspace{0.5em}
						\py[timetable_old]{tt}
						\py[timetable_new]{tt}
						\vspace{-1em}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Behaviour}
						Behavioural measurements can help stratify subjects and identify correlated fMRI activation patterns.
						Forced swim test results from the optogenetic responder cohort (figure~\ref{fig:fst_new}) spread over half the measurement range, and would thus be suitable to regress fMRI variance.
						Results from all binned cohorts (figure~\ref{fig:fst_all}) show a trend indicating that fluoxetine treatment may induce additional behaviour variance in healthy C57BL/6 mice undergoing longitudinal fMRI.
						\vspace{0.4em}
						\begin{center}
							\begin{minipage}{0.45\textwidth}
								\py[fst_new]{fig}
							\end{minipage}
							\hspace{1em}
							\begin{minipage}{0.45\textwidth}
								\py[fst_all]{fig}
							\end{minipage}
						\end{center}
						\vspace{-1em}
					\end{myblock}\vfill
					\vspace{-0.3em}
					\begin{myblock}{Outlook}
						\begin{itemize}
							\item More constrained region of interest analysis.
							\item Control group analysis to test for underlying (signal decay) trends.
							\item Population stratification according to forced swim and open field test scores.
							\item DR SNR improvement and true functional connectivity tracking.
						\end{itemize}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
	\begin{column}{.57\textwidth}
		\begin{beamercolorbox}[center]{postercolumn}
			\begin{minipage}{.98\textwidth} % tweaks the width, makes a new \textwidth
				\parbox[t][\columnheight]{\textwidth}{ % must be some better way to set the the height, width and textwidth simultaneously
					\begin{myblock}{MRI Methods}
						\vspace{0.5em}
						\begin{center}
							\begin{minipage}{.37\textwidth}
							Data Acquisition:
								\begin{itemize}
									\item Bruker PharmaScan (\SI{7}{\tesla}, \SI{16}{\centi\metre} bore)
									\item Implant-compatible in-house T/R coil
									\item Gradient-echo EPI:
										\begin{itemize}
											\item TR=\SI{1000}{\ms}, TE=\SI{5}{\ms}, FA=\SI{60}{\degree}
											\item x($\phi$)=\SI{312.5}{\um}, y($\nu$)=\SI{281.25}{\um}, z(slice)=\SI{500}{\um}
										\end{itemize}
									\item Endorem (Laboratoire Guebet SA)
								\end{itemize}
							\end{minipage}
							\begin{minipage}{.26\textwidth}
								Anesthesia Protocol \cite{Grandjean2014}:
								\begin{itemize}
									\item Free breathing, 1:4 O$_2$/air
									\item Bolus:
										\begin{itemize}
											\item \SI{0.05}{\mg\per\kg} Medetomidine
											\item \SI{1.5}{\percent} Isoflurane
										\end{itemize}
									\item Maintenance:
										\begin{itemize}
											\item \SI{0.1}{\mg\per\kg\per\hour} Medetomidine
											\item \SI{0.5}{\percent} Isoflurane
										\end{itemize}
								\end{itemize}
							\end{minipage}
							\begin{minipage}{.32\textwidth}
								Data Analysis:
								\begin{itemize}
									\item SAMRI (\href{https://github.com/IBT-FMI/SAMRI}{github.com/IBT-FMI/SAMRI}), internally using:
										\begin{multicols}{3}
											\begin{itemize}
												\item AFNI
												\item ANTs
												\item Bru2Nii
												\item FSL
												\item Matplotlib
												\item Nibabel
												\item Nilearn
												\item Nipy
												\item Nipype
												\item Pandas
												\item Seaborn
												\item Statsmodels
											\end{itemize}
										\end{multicols}
								\end{itemize}
							\end{minipage}
						\end{center}
					\end{myblock}\vfill
					\begin{myblock}{opto-fMRI}
						\begin{minipage}{.73\textwidth}
							We find the optogenetically elicited cerebral blood volume (CBV) response best modelled by applying a beta function convolution to the laser stimulus events, as depicted in figure~\ref{fig:ts}.
							\vspace{0.8em}

							Given the whole-brain statistic maps in figure~\ref{fig:maps}, we implement a rudimentary serotonergic network model, and select two weightings of interest (see figure~\ref{fig:dcm}) to demonstrate the longitudinal analysis capabilities of our protocol.
							The variables u$_1$ and u$_2$ are the weightings of interest for longitudinal analysis of serotonergic system excitability and transmission strength, respectively.
							Due to low signal to noise (SNR) in the DR, we use the artificial u$_3$ as a proxy for u$_2$.
						\end{minipage}
						\begin{minipage}{.04\textwidth}
						\end{minipage}
						\begin{minipage}{.23\textwidth}
							\vspace{0.4em}
							\begin{figure}
								\begin{minipage}{.94\textwidth}
									\centering\includegraphics[width=\textwidth]{img/dcm.png}
									\caption{Three-node representation of serotonergic system signal transfer during optogenetic stimulation.
									\label{fig:dcm}}
								\end{minipage}
							\end{figure}
						\end{minipage}
						\vspace{0.3em}
						\hspace{-0.5em} \py[ts]{fig}
						\begin{minipage}{.57\textwidth}
							\py[ofM]{fmri}\vspace{-1.8em}
							\py[ofM_aF]{fmri}\vspace{-1.8em}
							\py[ofM_cF1]{fmri}\vspace{-1.8em}
							\py[ofM_cF2]{fmri}\vspace{-1.8em}
							\py[ofM_pF]{fmri}\vspace{-1.8em}
						\end{minipage}
						\begin{minipage}{.04\textwidth}
						\end{minipage}
						\begin{minipage}{.39\textwidth}
							\py[dr_tc]{fig}\vspace{-.8em}
							\py[ctx_tc]{fig}
						\end{minipage}
						\vspace{0.9em}\\

						An exploratory repeated measures ANOVA omnibus test indicates no significance \py[ctx_tc]{anova} for the trend seen in figure~\ref{fig:ctx_tc}, whereas the trend of figure~\ref{fig:dr_tc} approaches a signicicance level of p=0.05 \py[dr_tc]{anova}.
						The tentative interpretation of these findings is that there is no significant evolution of serotonergic output to the cortex over the course of treatment, though there is a neigh-significant decrease in DR excitability.
						Two important caveats are that the observed trends may owe in whole or part to (or be obfuscated by) a decrease in virus expression, and that serotonergic transmission evolution may be significant for more localized regions of interest.
					\end{myblock}\vfill
					\begin{myblock}{References}
						\vspace{-0.8em}
						\begin{multicols}{2}
							\scriptsize
							\bibliographystyle{unsrt}
							% \bibliographystyle{abbrv}
							\bibliography{./bib}
						\end{multicols}
					\end{myblock}\vfill
		}\end{minipage}\end{beamercolorbox}
	\end{column}
\end{columns}
\end{frame}
\end{document}
